/*
Navicat MySQL Data Transfer

Source Server         : smile-test
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2017-06-22 16:35:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `uid` varchar(200) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `age` int(8) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', '21346', 'lisi', '456', '31', '13744552345');
INSERT INTO `user` VALUES ('4', '6545324', 'wangwu', '123', '27', '15898765443');
INSERT INTO `user` VALUES ('6', '123256', 'wert', 'jhdst', '33', 'df34567');
INSERT INTO `user` VALUES ('9', '2354674', 'wertyu', 'eyrerq', '33', 'rty34578');
INSERT INTO `user` VALUES ('10', '144356', 'zhangsan', '123', '22', 'r123456999');
INSERT INTO `user` VALUES ('13', 'fd3547b5-17c7-4c92-9c61-cc980e39a5b2', 'zhaoliu', 'ww123', '34', '15987654123');
INSERT INTO `user` VALUES ('15', '89fec767-2243-4661-9d4c-ca99b61cc58a', 'wangpinpin', '34568756434re', '33', '14569313444');
INSERT INTO `user` VALUES ('16', 'eb643aeb-27b4-4a21-84ae-2aa87debe5ab', 'yuretrer', 'wqrtuurete', '44', '12346782');
INSERT INTO `user` VALUES ('17', '195dc538-be8e-4d97-8c8f-6d4c4d9b7e91', '767655', '126cgdgf', '11', '67876543432');
INSERT INTO `user` VALUES ('18', '13d8ef06-277b-4156-ab5c-0204acfdabe7', 'bfsdr', '4577', '44', '2345677222');
INSERT INTO `user` VALUES ('19', 'afc37e99-27bd-494b-9cee-3a9df23e89ad', '774dssarwee', '24553', '12', '345932');
INSERT INTO `user` VALUES ('20', 'be61a52d-994e-4a9f-863c-246834b4ad5f', 'rlllllou', '14fdsesds', '25', '2357797532');
INSERT INTO `user` VALUES ('21', '07d6478c-4af0-4db2-be1b-b03e1f0a6f4d', '3306-ff', 'dddd', '33', '3679876521');
