<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增页面</title>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	
	$(function(){
		
		$("#save").click(function(){
			var username = $("#username").val();
			var password = $("#password").val();
			var age = $("#age").val();
			var phone = $("#phone").val();
			$("#addform").attr("action","user/add"); 
			$("#addform").submit();
			
			/*  $.ajax({
			     url: "user/add",
	             type: "POST",
	             data: {'username':username,'password':password,'age':age,'phone':phone},
	             dataType: "json",
				 async: false,//false同步，true为异步
	             success: function(data){
	            	 
	            	 if(data == 1){
	            		 alert("保存成功！");
	            		 window.location.href="javascript:history.back(-1);location.reload();";
	            	 }
                 }
	         }); */
		});
		
		
	});
	
</script>
<style type="text/css">
	h2{text-align: center;}
	div{text-align: center;}
</style>
</head>
<body>
	<h2>新增页面</h2>
	<hr>
	<form id="addform" name="addform" action="" method="post">
		<div>
			<label>用户名：</label><input type="text" style="width: 150px;" id="username" name="username" />&nbsp;&nbsp;&nbsp;
			<label>密&nbsp;&nbsp;&nbsp;码：</label><input type="text" id="password" name="password" style="width: 150px;" /><br/>
			<label>年&nbsp;&nbsp;&nbsp;龄：</label><input type="text" id="age" name="age" style="width: 150px;" />&nbsp;&nbsp;&nbsp;
			<label>电&nbsp;&nbsp;&nbsp;话：</label><input type="text" id="phone" name="phone" style="width: 150px;" /><br/><br/>
			<button type="button" id="save" >保存</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" onclick="window.location.href='javascript:history.back(-1);location.reload();'" >取消</button>
		</div>
	</form>
</body>
</html>