<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>成功页面</title>
<link rel="stylesheet" type="text/css" href="css/paging.css">
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/paging/query.js"></script>
<script type="text/javascript" src="js/paging/paging.js"></script>
<script type="text/javascript">
	$(function(){
		
		$("#tuichu").click(function(){
			$("#tuichuform").submit();
		});
	});
	
	
	function dele(node){
		var id = $(node).closest("tr").attr("id");
		if(confirm("确定要删除？")){
			$("#deleteform").attr("action","user/delete?id="+id+""); 
			$("#deleteform").submit();
		}
	}
</script>
<style type="text/css">
	body{ text-align:center} 
	h2{text-align: center;}
	#div0{width: 100%;height: 50px; text-align: center;}
	#div2{float: right;margin-top: -50px;}
	#div3{float: right;margin-top: -50px;margin-right: 50px;}
	table{text-align: center;}
	#div4{margin-left: 300px;}
</style>
</head>
<body>
	<form id="tuichuform" name="tuichuform" action="user/tuichu" method="post"></form>
	<form id="deleteform" name="deleteform" action="" method="post"></form>
	<h2>登陆成功</h2><br>
	<div id="div0">
		<div id="div1">
			<h3>欢迎您:${user.username }</h3>
		</div>
		<div id="div2">
			<button type="button" id="tuichu">退出</button>
		</div>
		<div id="div3">
			<button type="button" id="add" onclick="window.location.href='user/toadd'">增加</button>
		</div>
	</div>
	
	<hr>
	<div id="div4">
		<table width="800" border="1" cellspacing="0" id="usertable">
			<thead>
				<tr>
					<td>序号ID</td>
					<td>用户名</td>
					<td>密码</td>
					<td>年龄</td>
					<td>电话</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${list }" var="users" varStatus="rs">
					<tr id="${users.id }">
						<td>${rs.index+1 }</td>
						<td>${users.username }</td>
						<td>${users.password }</td>
						<td>${users.age }</td>
						<td>${users.phone }</td>
						<td><a href="user/toupdate?id=${users.id }">修改</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="dele(this);">删除</a></td>
					</tr>
				
				</c:forEach>
			</tbody>
		</table>
		<div id="pageTool"></div>
		<input type="hidden" value="${count }" id="count" name="count" />
		<input type="hidden" value="${pagesize }" id="pagesize" name="pagesize" />
		<script type="text/javascript">
		
			var count = $("#count").val();//初始化数据库的数据量
			var pagesize = $("#pagesize").val();//每页显示的数量
			
			$('#pageTool').Paging({pagesize:pagesize,count:count,toolbar:true,callback:function(page,size,pages){
				console.log(arguments);
				//alert('当前第 ' +page +'页,每页 '+size+'条,总页数：'+pages+'页');
				$("#usertable tbody").empty();
				$.ajax({
				     url: "user/findpageUser",
		             type: "POST",
		             data: {'page':page,'size':size},
		             dataType: "json",
					 async: false,//false同步，true为异步
		             success: function(data){
		            	
		            	$.each(data,function(e,v){
		            		var tr = "";
							tr = tr + "<tr id="+ v.id +">";
							tr = tr + "<td>"+((page-1)*size+e+1)+"</td>";
							tr = tr + "<td>"+ v.username +"</td>";
							tr = tr + "<td>"+ v.password +"</td>";
							tr = tr + "<td>"+ v.age +"</td>";
							tr = tr + "<td>"+ v.phone +"</td>";
							tr = tr + "<td><a href='user/toupdate?id="+ v.id +"'>修改</a>&nbsp;&nbsp;<a href='javascript:void(0);' onclick='dele(this);'>删除</a></td>";
		        			tr = tr + "</tr>";
		        			$("#usertable tbody").append(tr);
		            	});
		            	
	                 }
		         });
				
			}});
			/* $('#pageToolbar').Paging({pagesize:10,count:85,toolbar:true}); */
			
		</script>
	</div>
</body>
</html>