package com.cn.yu.service;

import java.util.List;
import java.util.Map;

import com.cn.yu.model.User;

public interface IUserService {

	//登陆
	public User islogin(User user);
	
	//查询所有对象
	public List<User> findall();
	
	//增加
	public int add(User user);
	
	//删除
	public int delete(Integer id);
	
	//根据id查询对象
	public User findbyid(Integer id);
	
	//修改
	public int update(User user);
	
	//判断登陆信息
	public User findbyuid(String uid);
	
	//统计行数
	public int findallcount();
	
	//分页查询
	public List<User> findallpage(Map<String, Object> map);
	
}
