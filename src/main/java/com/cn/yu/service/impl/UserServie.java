package com.cn.yu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cn.yu.service.IUserService;
import com.cn.yu.dao.IUserDao;
import com.cn.yu.model.User;

@Service
public class UserServie implements IUserService {

	@Autowired
	IUserDao iuserdao;
	
	@Override
	public User islogin(User user) {
		return iuserdao.islogin(user);
	}

	@Override
	public List<User> findall() {
		return iuserdao.findall();
	}

	@Override
	public int add(User user) {
		return iuserdao.add(user);
	}

	@Override
	public int delete(Integer id) {
		return iuserdao.delete(id);
	}
	
	@Override
	public User findbyid(Integer id) {
		return iuserdao.findbyid(id);
	}

	@Override
	public int update(User user) {
		return iuserdao.update(user);
	}

	@Override
	public User findbyuid(String uid) {
		return iuserdao.findbyuid(uid);
	}

	@Override
	public int findallcount() {
		return iuserdao.findallcount();
	}

	@Override
	public List<User> findallpage(Map<String, Object> map) {
		return iuserdao.findallpage(map);
	}


}
