package com.cn.yu.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cn.yu.model.User;
import com.cn.yu.service.IUserService;
import com.mysql.fabric.xmlrpc.base.Data;

@Controller
@RequestMapping(value="user")
public class UserController {
	
	@Autowired
	IUserService iUserService;

	//全局变量uid
	private String uid;
	
	@SuppressWarnings("unused")
	@RequestMapping(value="login")
	public String checklogin(User user, Model model,String username, String password){
		user.setUsername(username);
		user.setPassword(password);
		if(user == null){
			return "index";
		}
		user = iUserService.islogin(user);
		if(user != null){
			//登陆的时候给uid赋值，后面的增删改查都需要用uid来定位这个user
			uid = user.getUid();
			model.addAttribute("user", user);
			return "redirect:/user/findall";
		}else{
			return "index";
		}
		
	}
	
	@RequestMapping(value="findall")
	public String findAllUser(User user, Model model){
		
		int count = iUserService.findallcount();
		System.out.println("总共有用户数:"+count);
		model.addAttribute("count", count);
		model.addAttribute("pagesize", "5");
		
		List<User> list = iUserService.findall();
		model.addAttribute("list", list);
		//根据uid定位user
		System.out.println("uid:"+uid);
		user = iUserService.findbyuid(uid);
		model.addAttribute("user", user);
		return "success";
	}
	
	@RequestMapping(value="findpageUser")
	@ResponseBody
	public String findpageUser(User user,String page, String size, Model model){
		
		Map<String, Object> map = new HashMap<String, Object>();
		int start = Integer.valueOf(page);
		int pagesize = Integer.valueOf(size);
		start = (start-1)*pagesize;
		map.put("start",start);//当前页数
		System.out.println("当前页数："+(start/pagesize+1));
		
        map.put("size",pagesize); //每页有多少条
        System.out.println("每页条数："+pagesize);
        
		List<User> list = iUserService.findallpage(map);
		System.out.println("list长度："+list.size());
		for(int i = 0;i < list.size();i++){
			System.out.println(list.get(i).getUsername()+","+list.get(i).getPassword());
		}
		String success = JSON.toJSONString(list);
		return success;
	}
	
	@RequestMapping(value="tuichu")
	public String tuiChu(HttpSession session){
		System.out.println("退出登陆");
		/*清楚该用户session*/
		session.invalidate();
		return "index";
	}
	
	@RequestMapping(value="toadd")
	public String toAdd( Model model){
		return "add";
	}
	
	@RequestMapping(value="add")
	public String add(String username, String password, String age, String phone,User user, Model model){
		System.out.println("增加");
		String uidadd = UUID.randomUUID().toString()+(new Data()).toString();
		user.setUid(uidadd);
		user.setUsername(username);
		user.setPassword(password);
		user.setAge(Integer.valueOf(age));
		user.setPhone(phone);
		int flag = iUserService.add(user);
		if(flag > 0){
			//根据uid定位user
			System.out.println("uid-add:"+uid);
			user = iUserService.findbyuid(uid);
			model.addAttribute("user", user);
			return "redirect:/user/findall";//重定向跳转
			//return "forward:/user/findall";//内部跳转
		}else{
			return "error";
		}
	}
	
	@RequestMapping(value="delete")
	public String delete(String id, Model model, User user){
		int dele = iUserService.delete(Integer.valueOf(id));
		if(dele > 0){
			//根据uid定位user
			System.out.println("uid-delete:"+uid);
			user = iUserService.findbyuid(uid);
			model.addAttribute("user", user);
			return "redirect:/user/findall";
		}else{
			return "error";
		}
		
	}
	
	@RequestMapping(value="toupdate")
	public String toupdate(String id, Model model, User user){
		user = iUserService.findbyid(Integer.valueOf(id));
		model.addAttribute("user", user);
		return "update";
		
	}
	
	@RequestMapping(value="update")
	public String update(String udid, String username, String password, String age, String phone, Model model, User user){
		user.setId(Integer.valueOf(udid));
		user.setUsername(username);
		user.setPassword(password);
		user.setAge(Integer.valueOf(age));
		user.setPhone(phone);
		int update = iUserService.update(user);
		
		if(update > 0){
			//根据uid定位user
			System.out.println("uid-update:"+uid);
			user = iUserService.findbyuid(uid);
			model.addAttribute("user", user);
			return "redirect:/user/findall";
		}else{
			return "error";
		}
		
	}

}
